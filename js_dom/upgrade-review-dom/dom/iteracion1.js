/* 1.1 Basandote en el array siguiente, crea una lista ul > li 
dinámicamente en el html que imprima cada uno de los paises. */

const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

const $$ul = document.createElement("ul");
$$ul.className = 'ul1'
document.body.appendChild($$ul)

function createLi (arr, dir) {
	for (let i = 0; i < arr.length; i++) {
		const el = arr[i];
		const $$li = document.createElement("li");
		$$li.innerText = el;
		document.querySelector(dir).appendChild($$li);
	}
}
createLi(countries, '.ul1');

/* 1.2 Elimina el elemento que tenga la clase .fn-remove-me.
 */
document.querySelector('.fn-remove-me').remove();


/* 1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos 
en el div de html con el atributo data-function="printHere". */

const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

const $$ul1 = document.createElement('ul');
$$ul1.className = 'ul2';
document.querySelector('[data-function="printHere"]').appendChild($$ul1);

createLi(cars, '.ul2')


/* 1.4 Crea dinamicamente en el html una lista de div que contenga un elemento 
h4 para el titulo y otro elemento img para la imagen. */

const countries0 = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

const myList = document.createElement('div');
myList.className = ('myList');
document.body.appendChild(myList);

function createDivH4Img (arr) {
	for (let i = 0; i < arr.length; i++) {
		const el = arr[i];
		const $$div = document.createElement("div");
		$$div.className = 'contDiv'
		document.querySelector('.myList').appendChild($$div)
		
		const $$img = document.createElement("img");
		$$img.className = 'contDiv__img'
		$$img.src = el.imgUrl;
		$$img.alt = 'img';
		$$div.appendChild($$img);

		const $$h4 = document.createElement('h4');
		$$h4.className = 'contDiv__title';
		$$h4.textContent = el.title;
		$$div.appendChild($$h4)

	}
}
createDivH4Img(countries0);



/* 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último 
elemento de la lista. */

const removeLast = () => {
	const select = document.querySelector('.myList');
	select.removeChild(select.lastChild);
}
const createButton = () =>{
	const $$button = document.createElement('button');
	$$button.className = 'firstButton';
	$$button.textContent = 'remove last child'
	document.body.appendChild($$button);

	$$button.addEventListener('click', removeLast);
}
createButton();

/* 1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los 
elementos de las listas que elimine ese mismo elemento del html.
 */


const remove = () => {
	document.querySelector().parentNode.remove();
}

const myListElem = document.querySelectorAll('.contDiv');

for (let i = 0; i < myListElem.length; i++) {
	const el = myListElem[i].childNodes;
	el.forEach((element,i1) => {
		const $$bts = document.createElement('button'); 
		$$bts.className = 'btn__remove'+ i +i1; 
		$$bts.textContent = 'remove__content';
		element.appendChild($$bts)
		$$bts.addEventListener("click", remove);
	});
}
/* 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el 
evento click que ejecute un console log con la información del evento del click */
const btnFunction = (event) =>{
    console.log(event);
}

const newButton = () => {
    const button = document.createElement('button');
    button.id = 'btnToClick'
    button.textContent = "BUTTON"
    document.body.appendChild(button)
}
newButton();

document.getElementById("btnToClick").addEventListener("click", btnFunction);

/* 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input. */

const inputTarget = document.querySelector('.focus')
inputTarget.addEventListener ('focus', (event) => {
    event.target.style.background = 'red';
  });


/* 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input. */

document.querySelector(".value").addEventListener("input", () => {
    console.log('Ha cambiado el contenido del input!');
  });

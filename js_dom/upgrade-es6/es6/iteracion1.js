/* Crea una arrow function que tenga dos parametros a y b y 
que por defecto el valor de a = 10 y de b = 5. Haz que la función muestre 
por consola la suma de los dos parametros. */

//1.1 Ejecuta esta función sin pasar ningún parametro

const sum = (a=10, b=5) =>{
    return a+b ;
}
const suma = sum()
console.log(suma)

//1.2 Ejecuta esta función pasando un solo parametro

const sum1 = sum(1);
console.log(sum1)

//1.3 Ejecuta esta función pasando dos parametros

const sum2 = sum(1,100);
console.log(sum2)
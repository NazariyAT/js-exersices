/* Crea una función llamada `swap()` que reciba un array y dos parametros que sean indices del array. La función deberá intercambiar la posición de los valores de los indices que hayamos enviado como parametro. Retorna el array resultante.

Sugerencia de array: */

const arr = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño']


function swap(input, indice1, indice2) {
    let temp = input[indice1];
    input[indice1] = input[indice2];
    input[indice2] = temp;
}

swap(arr, 1, 2);
console.log(arr);